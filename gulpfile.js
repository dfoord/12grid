'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');

gulp.task('sass', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist'));
});

gulp.task('minify-css', ['sass'], () => {
  return gulp.src('dist/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename((path) => {
        path.basename += '.min';
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('example', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./example'));
});

gulp.task('dist', ['sass', 'minify-css']);